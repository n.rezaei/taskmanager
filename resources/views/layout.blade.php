<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>@yield('title', 'مدیریت تسک ها')</title>

        <link rel="stylesheet" href="{{asset('css/bootstrap.rtl.min.css')}}" >

    </head>
    <body class="antialiased">
        <div class="container">
            @yield('content')
        </div>
    </body>
</html>
<script src="{{asset('js/jquery-3.6.0.min.js')}}"></script>
<script src="{{asset('js/bootstrap.bundle.min.js')}}" ></script>
@yield('js')
