@extends('layout')

@section('title', 'نمایش وظایف')

@section('content')
    <h5 class="text-center">ایجاد تسک وظایف</h5>
    <div class="row">
        <div class="col-4">
            <a href="{{route('tasks.index')}}" class="btn btn-primary float-left">نمایش لیست وظایف</a>
        </div>
    </div>
    <div class="row mt-2">
        <div class="col-4">
            {{ Form::open(['url' => route('tasks.store'), 'method' => 'post']) }}
                {{ Form::label('title', 'عنوان') }}
                {{ Form::text('title', null, ['class' => 'form-control', 'required']) }}
                {{ Form::submit('ذخیره', ['class' => 'btn btn-success mt-2', 'style' => 'float:left']) }}
            {{ Form::close() }}
        </div>
    </div>
@endsection
