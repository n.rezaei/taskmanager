@extends('layout')

@section('title', 'نمایش وظایف')

@section('content')
    <h5 class="text-center">نمایش وظایف</h5>
    <div class="row">
        <div class="col-2">
            <a href="{{route('tasks.create')}}" class="btn btn-primary float-left">ایجاد تسک جدید</a>
        </div>
    </div>
    <div class="row mt-2">
        <div class="col-8">
            <ul class="list-group">
                @forelse($tasks as $task)
                    <li class="list-group-item" dir="rtl">
                        <div class="row">
                            <div class="col-4">
                                <div class="font-weight-bold">{{$task->title}}</div>
                            </div>
                            <div class="col-4">
                                {{ Form::label('status', 'انجام شده') }}
                                {{ Form::checkbox('status', null , $task->status_task, ['id'=>'status_task', 'task_id' => $task->id]) }}
                            </div>
                            <div class="col-4">
                                <a href="{{route('tasks.edit', $task->id)}}" class="btn btn-info">ویرایش</a>

                                {!! Form::open(['method'=>'DELETE', 'url' =>route('tasks.destroy', $task->id),'style' => 'display:inline']) !!}
                                {!! Form::button('حذف', array('type' => 'submit','class' => 'btn btn-danger','onclick'=>'return confirm("مطمئنی حذف کنی؟")')) !!}
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </li>
                @empty
                    <span>تسکی برای نمایش وجود ندارد.</span>
                @endforelse
            </ul>
        </div>
    </div>
@endsection

@section('js')
    <script>
        $(document).ready(function () {
            $('body').on('click', '#status_task', function () {
                let check = $(this).prop("checked") ? 1 : 0;
                var task_id = $(this).attr('task_id');
                $.ajax({
                    url: "changeStatus/"+task_id,
                    data: {'status':check },
                    type: 'GET',
                    success: function (result) {
                    }
                });
            })
        })
    </script>
@endsection
