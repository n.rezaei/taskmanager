<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    use HasFactory;

    protected $fillable = ['title', 'status'];

    /**
     * @return string|null
     */
    public function getStatusTaskAttribute(): ?string
    {
        return $this->status ? 'checked' : null;
    }
}
